package DAO;

import Entities.ExerciseEntity;
import java.util.List;

import java.util.Collection;

/**
 * Created by dangdien on 4/14/17.
 */
public interface ExerciseDAO {
    List<ExerciseEntity> findAllExercise();
    void deleteExercise();
    void insertExercise(ExerciseEntity ex);
    void updateExercise();
}
