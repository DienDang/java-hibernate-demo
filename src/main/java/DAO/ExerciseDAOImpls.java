package DAO;

import Bootstraping.HibernateBoostraping;
import Entities.ExerciseEntity;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import java.util.List;

import java.util.Collection;

/**
 * Created by dangdien on 4/14/17.
 */
public class ExerciseDAOImpls implements ExerciseDAO {
    public void insertExercise (ExerciseEntity ex) {
        Session mySession = HibernateBoostraping.getSession();
        try {
            Transaction tx = mySession.beginTransaction();
            ExerciseEntity myEx = new ExerciseEntity();
            myEx.setName(ex.getName());
            myEx.setDevice(ex.getDevice());
            myEx.setGuide(ex.getGuide());
            myEx.setVideo(ex.getVideo());

            mySession.save(ex);
            tx.commit();
        }
        catch (Exception e){
            System.out.println("Fail to append new record");
            e.printStackTrace();
        }
        finally {
            mySession.close();
        }
    }

    public List<ExerciseEntity> findAllExercise() {
        Session mySession = HibernateBoostraping.getSession();
        String hql = "FROM ExerciseEntity";
        Query query = mySession.createQuery(hql);
        return  query.list();
    }

    public void deleteExercise(){}
    public void updateExercise(){}
}
