package Mains;

import Bootstraping.HibernateBoostraping;
import Entities.ExerciseEntity;
import Entities.ImagesEntity;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dangdien on 4/15/17.
 */
public class ManyToOne {
    public static void main(String[] args) {
        Session mySession = HibernateBoostraping.getSession();
        Transaction tx = mySession.beginTransaction();
        ExerciseEntity ex = mySession.get(ExerciseEntity.class, 15);
//        ExerciseEntity ex = new ExerciseEntity("name", "device", "guide", "video");
  //      ImagesEntity e1 = new ImagesEntity("link1");
    //    e1.setExId(ex);


        List<ImagesEntity> images = new ArrayList<ImagesEntity>();
        images.add(new ImagesEntity("1323"));
        images.add(new ImagesEntity("124345"));


        ex.setImages(images);

//        ImagesEntity img = new ImagesEntity("link2");
//        img.setEx_Id(ex);


        mySession.merge(ex);
        tx.commit();
    }
}
