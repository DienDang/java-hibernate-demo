package Mains;

import Bootstraping.HibernateBoostraping;
import DAO.ExerciseDAOImpls;
import Entities.*;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.hibernate.metadata.ClassMetadata;

import java.util.ArrayList;
import java.util.Map;
import java.util.SortedMap;
import java.util.List;

/**
 * Created by dangdien on 4/13/17.
 */
public class ManyToMany {


    public static void main(final String[] args) throws Exception {
        ExerciseDAOImpls dao = new ExerciseDAOImpls();
        List<ImagesEntity> newImage = new ArrayList<ImagesEntity>();
        Session mySession = HibernateBoostraping.getSession();
        Transaction tx = mySession.beginTransaction();

      CustomerEntity cus1 = new CustomerEntity("cus1");
        CustomerEntity cus2 = new CustomerEntity("cus2");
        List<CustomerEntity> cusList = new ArrayList<CustomerEntity>();
        cusList.add(cus1);
        cusList.add(cus2);


//        List<PlanEntity> plans = new ArrayList<PlanEntity>();
        PlanEntity plan1 = new PlanEntity("plan1");
        plan1.setCustomers(cusList);
//        PlanEntity plan2 = new PlanEntity("plan2");
//        plans.add(plan1);
//        plans.add(plan2);

//        cus1.setPlans(plans);
//        cus2.setPlans(plans);

//        ExerciseEntity ex = mySession.get(ExerciseEntity.class, 9);
//        ImagesEntity img = new ImagesEntity("this is link field");
//        img.setEx_Id(ex);
//        mySession.persist(img);

//        img.setEx_Id(ex);
//        newImage.add(img);
//        ex.setImages(newImage);


//        ImagesEntity img = new ImagesEntity("youtubee.com");
//        DescriptionEntity des = new DescriptionEntity("this is an image", img);
//        img.setDescription(des);
//        ImagesEntity img = mySession.get(ImagesEntity.class, 10);
//        img.setEx_Id(ex);

        mySession.persist(plan1);
        tx.commit();

//        ImagesEntity newImage = new ImagesEntity("google.com");
////        newImage.setEx_Id(2);
//        Session mySession = HibernateBoostraping.getSession();
//        Transaction tx = mySession.beginTransaction();
//        ExerciseEntity exerciseEntity=   mySession.get(ExerciseEntity.class,2);
//        System.out.println(exerciseEntity);
//        newImage.setEx_Id(exerciseEntity);
//
//        mySession.persist(newImage);
//        tx.commit();
        

    }
}