package Entities;

import sun.security.krb5.internal.crypto.Des;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by dangdien on 4/14/17.
 */
@Entity
@Table(name = "images")
public class ImagesEntity {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Basic
    @Column(name = "link")
    private String link;

    @ManyToOne(cascade = CascadeType.PERSIST, targetEntity = ExerciseEntity.class)
    @JoinColumn(name = "ex_id", referencedColumnName = "id")
    private ExerciseEntity exId;

    @OneToOne(mappedBy = "image", cascade = CascadeType.ALL)
    private DescriptionEntity description;

    public DescriptionEntity getDescription() {
        return description;
    }

    public void setDescription(DescriptionEntity description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public ExerciseEntity getExId() {
        return exId;
    }

    public void setExId(ExerciseEntity exId) {
        this.exId = exId;
    }

    public ImagesEntity(String link) {
        this.link = link;
    }

    public ImagesEntity() {
    }
}
