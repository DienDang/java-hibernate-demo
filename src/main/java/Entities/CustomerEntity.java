package Entities;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Customer")
public class CustomerEntity {
    @Basic
    @Column(name = "name")
  private String name;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
  private int id;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "customer_plan", joinColumns = {
            @JoinColumn(name = "customer_id")},
            inverseJoinColumns = {
                    @JoinColumn(name = "Plan_id")
            })private List<PlanEntity> plans;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

    public CustomerEntity(String name) {
        this.name = name;
    }

    public CustomerEntity() {
    }

    public List<PlanEntity> getPlans() {
        return plans;
    }

    public void setPlans(List<PlanEntity> plans) {
        this.plans = plans;
    }

    public CustomerEntity(String name, List<PlanEntity> plans) {
        this.name = name;
        this.plans = plans;
    }
}
