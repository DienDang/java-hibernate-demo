package Entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by dangdien on 4/13/17.
 */
@Entity
@Table(name = "exercise")
public class ExerciseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Basic
    @Column(name = "name")
    private String name;
    @Basic
    @Column(name = "device")
    private String device;
    @Basic
    @Column(name = "guide")
    private String guide;
    @Basic
    @Column(name = "video")
    private String video;

    @OneToMany(mappedBy = "exId",cascade = CascadeType.ALL, targetEntity = ImagesEntity.class)
    private List<ImagesEntity> images;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getGuide() {
        return guide;
    }

    public void setGuide(String guide) {
        this.guide = guide;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public List<ImagesEntity> getImages() {
        return images;
    }

    public void setImages(List<ImagesEntity> images) {
        this.images = images;

//        for(ImagesEntity im: images) {
//            im.setExId(this);
//        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ExerciseEntity that = (ExerciseEntity) o;

        if (id != that.id) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (device != null ? !device.equals(that.device) : that.device != null) return false;
        if (guide != null ? !guide.equals(that.guide) : that.guide != null) return false;
        if (video != null ? !video.equals(that.video) : that.video != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (device != null ? device.hashCode() : 0);
        result = 31 * result + (guide != null ? guide.hashCode() : 0);
        result = 31 * result + (video != null ? video.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ExerciseEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", device='" + device + '\'' +
                ", guide='" + guide + '\'' +
                ", video='" + video + '\'' +
                '}';
    }

    public ExerciseEntity(String name, String device, String guide, String video) {
        this.name = name;
        this.device = device;
        this.guide = guide;
        this.video = video;
    }

    public ExerciseEntity() {
    }
}
