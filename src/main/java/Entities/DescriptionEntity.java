package Entities;

import javax.persistence.*;

/**
 * Created by dangdien on 4/15/17.
 */
@Entity
@Table(name = "Description")
public class DescriptionEntity {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Basic
    @Column(name = "description")
    private String description;

    @OneToOne
    @JoinColumn(name = "image_id")
    private ImagesEntity image;

    public DescriptionEntity(String description, ImagesEntity image) {
        this.description = description;
        this.image = image;
    }

    public DescriptionEntity() {
    }

    public int getId() {
        return id;
    }

    public DescriptionEntity(String description) {
        this.description = description;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ImagesEntity getImage() {
        return image;
    }

    public void setImage(ImagesEntity image) {
        this.image = image;
    }
}
