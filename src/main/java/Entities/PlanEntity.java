package Entities;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "plan")
public class PlanEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private int id;
  @Basic
  @Column(name = "name")
  private String name;

  @ManyToMany(cascade = CascadeType.ALL, targetEntity = CustomerEntity.class, fetch = FetchType.EAGER)
  @JoinTable(name = "customer_plan", joinColumns = {
          @JoinColumn(name = "Plan_id")},
          inverseJoinColumns = {
          @JoinColumn(name = "customer_id")
          })
  private List<CustomerEntity> customers;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

    public PlanEntity(String name) {
        this.name = name;
    }

    public PlanEntity() {
    }

    public List<CustomerEntity> getCustomers() {
        return customers;
    }

    public void setCustomers(List<CustomerEntity> customers) {
        this.customers = customers;
    }

    public PlanEntity(String name, List<CustomerEntity> customers) {
        this.name = name;
        this.customers = customers;
    }
}
