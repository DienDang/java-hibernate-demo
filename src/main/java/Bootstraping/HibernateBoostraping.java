package Bootstraping;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.*;

/**
 * Created by dangdien on 4/14/17.
 */
public class HibernateBoostraping {
    private static SessionFactory ourSessionFactory;

    public static SessionFactory config() {
        if(ourSessionFactory!=null)
        {
            return ourSessionFactory;
        }
        try {
            Configuration configuration = new Configuration();
            configuration.addAnnotatedClass(Entities.ExerciseEntity.class);
            configuration.addAnnotatedClass(Entities.ImagesEntity.class);
            configuration.addAnnotatedClass(Entities.DescriptionEntity.class);
            configuration.addAnnotatedClass(Entities.CustomerEntity.class);
            configuration.addAnnotatedClass(Entities.PlanEntity.class);

            configuration.configure();
            return ourSessionFactory = configuration.buildSessionFactory();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
//            return null;
        }
    }

    public static Session getSession() throws HibernateException {
        return config().openSession();
    }
}
